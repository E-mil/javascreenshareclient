package functionality;

public class ImageMonitor {
	
	private byte[][] packages;
	private int pkgPointer;
	private int width, height;
	private int blockHeight, blockWidth;
	private int rows, cols;
	
	public ImageMonitor() {
		packages = new byte[3][];
		pkgPointer = 0;
	}

	public synchronized void addBytePackage(byte[] msg) {
		packages[pkgPointer] = msg;
		pkgPointer++;
		notifyAll();
		while (pkgPointer == 3) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			}
		}
	}

	public synchronized byte[] getBytePackage() {
		while(pkgPointer == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return null;
			}
		}
		notifyAll();
		return packages[--pkgPointer];
	}
	
	public synchronized void setBlockHeight(int blockHeight) {
		this.blockHeight = blockHeight;
	}
	
	public synchronized void setBlockWidth(int blockWidth) {
		this.blockWidth = blockWidth;
	}
	
	public synchronized void setRows(int rows) {
		this.rows = rows;
	}
	
	public synchronized void setCols(int cols) {
		this.cols = cols;
	}
	
	public synchronized void setWidth(int width) {
		this.width = width;
	}
	
	public synchronized void setHeight(int height) {
		this.height = height;
	}

	public synchronized int getBlockHeight() {
		while(blockHeight == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return blockHeight;
	}

	public synchronized int getBlockWidth() {
		while(blockWidth == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return blockWidth;
	}

	public synchronized int getRows() {
		while(rows == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return rows;
	}

	public synchronized int getCols() {
		while(cols == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return cols;
	}

	public synchronized int getWidth() {
		while(width == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return width;
	}
	
	public synchronized int getHeight() {
		while(height == 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return 0;
			}
		}
		return height;
	}
}
