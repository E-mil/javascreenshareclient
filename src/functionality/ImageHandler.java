package functionality;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import user_interface.ImageDisplayScreen;

public class ImageHandler implements Runnable {
	
	private ImageMonitor monitor;
	private int rows, cols;
	private int[] pixels;
	private int blockHeight, blockWidth;
	private int width, height;

	public ImageHandler(ImageMonitor mon) {
		monitor = mon;
	}

	@Override
	public void run() {
		rows = monitor.getRows();
		cols = monitor.getCols();
		blockHeight = monitor.getBlockHeight();
		blockWidth = monitor.getBlockWidth();
		width = monitor.getWidth();
		height = monitor.getHeight();
		pixels = new int[rows*cols*blockWidth*blockHeight];
		if (Thread.currentThread().isInterrupted())
			return;
		
		byte[] msg;
		ImageDisplayScreen display = new ImageDisplayScreen();
		while (!Thread.currentThread().isInterrupted()) {
			msg = monitor.getBytePackage();
			if (Thread.currentThread().isInterrupted())
				break;
			
			//---decode message--- CONTENTS ==> :::(index of changed block : changed block) * nbr of changed blocks:::
			int n = 0;
			while (n < msg.length) {
				int blockNbr = 0;
				blockNbr |= (msg[n] << 24) & 0xff000000;
				n++;
				blockNbr |= (msg[n] << 16) & 0xff0000;
				n++;
				blockNbr |= (msg[n] << 8) & 0xff00;
				n++;
				blockNbr |= msg[n] & 0xff;
				n++;
				int x = blockNbr % cols;
				int y = blockNbr / cols;
				for(int i = 0; i < blockHeight; i++) {
					for (int j = 0; j < blockWidth; j++) {
						int index = y*blockHeight*width+i*width+x*blockWidth+j;
						pixels[index] = 0;
						pixels[index] |= (msg[n] << 16) & 0xff0000;
						n++;
						pixels[index] |= (msg[n] << 8) & 0xff00;
						n++;
						pixels[index] |= msg[n] & 0xff;
						n++;
					}
				}
			}
			
			DataBufferInt buffer = new DataBufferInt(pixels, pixels.length);
			int[] bmsks = new int[3];
			bmsks[0] = 0x00FF0000;
			bmsks[1] = 0x0000FF00;
			bmsks[2] = 0x000000FF;
			WritableRaster raster = Raster.createPackedRaster(buffer, width, height, width, bmsks, null);
			DirectColorModel dcm = new DirectColorModel(24, 0x00FF0000, 0x0000FF00, 0x000000FF);
			BufferedImage img = new BufferedImage(dcm, raster, false, null);
									
			display.updateImage(img.getScaledInstance(display.getWidth(), display.getHeight(), Image.SCALE_SMOOTH));
		}
		display.close();
	}
}
