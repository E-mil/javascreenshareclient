package communication;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;

import functionality.ImageMonitor;

public class ConnectionHandler implements Runnable {

	private Thread listener;
	private Thread writer;
	private Socket sock;
	private SocketAddress address;
	private ImageMonitor monitor;

	public ConnectionHandler(Socket s, SocketAddress sa, ImageMonitor mon) {
		sock = s;
		address = sa;
		monitor = mon;
	}

	private void start() {
		try {
			sock.connect(address);
		} catch (IOException e) {
			return;
		}
		listener = new Thread(new SocketListener(sock, monitor));
		writer = new Thread(new SocketWriter(sock));
		listener.start();
		writer.start();
	}

	private void stop() {
		if (listener != null)
			listener.interrupt();
		if (writer != null)
			writer.interrupt();
		try {
			if (listener != null)
				listener.join();
			if (writer != null)
				writer.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			sock.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		start();
		synchronized (this) {
			try {
				wait();
			} catch (InterruptedException e) {
				stop();
			}
		}
	}
}
