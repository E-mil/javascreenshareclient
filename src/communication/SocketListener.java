package communication;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import functionality.ImageMonitor;

public class SocketListener implements Runnable {

	private InputStream in;
	private ImageMonitor monitor;

	public SocketListener(Socket s, ImageMonitor mon) {
		try {
			in = s.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		monitor = mon;
	}

	@Override
	public void run() {
		//get some initial values from server and store in the monitor
		int width = 0, height = 0, blockWidth = 0, blockHeight = 0, rows = 0, cols = 0;
		try {
			for (int i = 0; i < 4; i++)
				width |= in.read() << (24 - i * 8);
			for (int i = 0; i < 4; i++)
				height |= in.read() << (24 - i * 8);
			for (int i = 0; i < 4; i++)
				blockWidth |= in.read() << (24 - i * 8);
			for (int i = 0; i < 4; i++)
				blockHeight |= in.read() << (24 - i * 8);
			for (int i = 0; i < 4; i++)
				rows |= in.read() << (24 - i * 8);
			for (int i = 0; i < 4; i++)
				cols |= in.read() << (24 - i * 8);
			System.out.println("W: " + width);
			System.out.println("H: " + height);
			System.out.println("BW: " + blockWidth);
			System.out.println("BH: " + blockHeight);
			System.out.println("R: " + rows);
			System.out.println("C: " + cols);
			monitor.setWidth(width);
			monitor.setHeight(height);
			monitor.setBlockWidth(blockWidth);
			monitor.setBlockHeight(blockHeight);
			monitor.setRows(rows);
			monitor.setCols(cols);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int len;
		while (!Thread.currentThread().isInterrupted()) {
			len = 0;
			for (int i = 0; i < 4; i++) {
				try {
					int temp = in.read();
					len |= temp << (24 - i * 8);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			byte[] msg = new byte[len];
			int sum = 0;
			while(sum < len) {
				try {
					sum += in.read(msg, sum, len - sum);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			monitor.addBytePackage(msg);
		}
	}
}
