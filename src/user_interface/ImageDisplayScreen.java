package user_interface;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ImageDisplayScreen extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4021062870269875611L;
	
	private Image img;
	private JFrame frame;
	
	public ImageDisplayScreen() {
		setPreferredSize(new Dimension(1000, 600));
		frame = new JFrame("Video feed");
		frame.getContentPane().add(this);
		frame.pack();
		frame.setVisible(true);
	}

	public void updateImage(Image image) {
		this.img = image;
		repaint();
	}
	
	public void close() {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				frame.dispose();
			}
		});
		t.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (img != null) {
			g.drawImage(img, 0, 0, null);
		}
	}
}
