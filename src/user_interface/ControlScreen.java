package user_interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import main.Main;

public class ControlScreen {
	
	private JFrame frame;
	private JButton connectBtn, disconnectBtn;
	private JTextField addressField, portField;
	private JLabel mainInfoLabel, addressInfoLabel, portInfoLabel;
	private Main main;
	
	public ControlScreen(Main m) {
		main = m;
		frame = new JFrame("Client");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setPreferredSize(new Dimension(400, 200));
		frame.setResizable(false);
		frame.setLocation(new Point(500, 300));
		
		JPanel topPane = new JPanel();
		topPane.setLayout(new BoxLayout(topPane, BoxLayout.Y_AXIS));
		topPane.setBackground(new Color(255, 196, 188));
		
		mainInfoLabel = new JLabel("Client for connecting to a JavaScreenshareServer");
		mainInfoLabel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 10));
		topPane.add(mainInfoLabel);
		
		addressInfoLabel = new JLabel("Enter server address:");
		topPane.add(addressInfoLabel);
		
		addressField = new JTextField();
		addressField.setSize(new Dimension(100, 15));
		topPane.add(addressField);
		
		portInfoLabel = new JLabel("Enter server port:");
		topPane.add(portInfoLabel);
		
		portField = new JTextField();
		portField.setSize(new Dimension(100, 15));
		portField.setText("12345");
		topPane.add(portField);
		
		JPanel btnContainer = new JPanel();
		btnContainer.setLayout(new BoxLayout(btnContainer, BoxLayout.X_AXIS));
		
		connectBtn = new JButton("Connect to server");
		disconnectBtn = new JButton("Disconnect from server");
		disconnectBtn.setEnabled(false);
		
		connectBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				connectBtn.setEnabled(false);
				main.startClient(addressField.getText(), Integer.parseInt(portField.getText()));
				disconnectBtn.setEnabled(true);
			}
		});
		
		disconnectBtn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				disconnectBtn.setEnabled(false);
				main.stopClient();
				connectBtn.setEnabled(true);
			}
		});
		
		btnContainer.add(connectBtn);
		btnContainer.add(disconnectBtn);
		btnContainer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		btnContainer.setBackground(new Color(255, 196, 188));
		
		JPanel pane = (JPanel) frame.getContentPane();
		pane.add(topPane, BorderLayout.NORTH);
		pane.add(btnContainer, BorderLayout.CENTER);
		
		frame.pack();
		frame.setVisible(true);
	}
}
