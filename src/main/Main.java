package main;

import java.net.InetSocketAddress;
import java.net.Socket;

import communication.ConnectionHandler;
import functionality.ImageHandler;
import functionality.ImageMonitor;
import user_interface.ControlScreen;

public class Main {

	private Thread connectionHandler;
	private Thread imageHandler;
	private ImageMonitor monitor;
	private Socket sock;

	public static void main(String[] args) {
		new Main();
	}

	public Main() {
		monitor = new ImageMonitor();
		new ControlScreen(this);
	}

	public void startClient(String address, int port) {
		sock = new Socket();
		
		connectionHandler = new Thread(new ConnectionHandler(sock, new InetSocketAddress(address, port), monitor));
		imageHandler = new Thread(new ImageHandler(monitor));
		
		connectionHandler.start();
		imageHandler.start();
	}
	
	public void stopClient() {
		connectionHandler.interrupt();
		imageHandler.interrupt();
		try {
			connectionHandler.join();
			imageHandler.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
